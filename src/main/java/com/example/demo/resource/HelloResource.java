package com.example.demo.resource;

import com.example.demo.entity.QSampleEntity;
import com.example.demo.entity.mapper.SampleEntityToVO;
import com.example.demo.entity.repository.SampleRepository;
import com.example.demo.vo.SampleVO;
import com.querydsl.core.types.dsl.BooleanExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class HelloResource {

    @GetMapping("/hello/{id}")
    public SampleVO getSampleData(@PathVariable Long id){
        Assert.notNull(id, "Id is null!");
        BooleanExpression eq =
                QSampleEntity.sampleEntity.id.eq(id);

        return sampleRepository.findOne(eq)
                .map( e-> sampleEntityToVO.toVo(e))
                .orElse(new SampleVO());

    }

    @Autowired
    SampleRepository sampleRepository;

    @Autowired
    SampleEntityToVO sampleEntityToVO;

}
