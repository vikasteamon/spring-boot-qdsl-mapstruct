package com.example.demo;

import com.example.demo.entity.SampleEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@SpringBootApplication
public class SpringBootQdslMapstructApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootQdslMapstructApplication.class, args);
	}

}

