package com.example.demo.entity.repository;

import com.example.demo.entity.SampleEntity;
import com.querydsl.apt.jpa.JPAAnnotationProcessor;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;

public interface SampleRepository extends CrudRepository<SampleEntity, Long>,
        QuerydslPredicateExecutor<SampleEntity> {
}
