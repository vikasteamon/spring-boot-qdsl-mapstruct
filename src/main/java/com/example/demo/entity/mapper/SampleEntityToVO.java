package com.example.demo.entity.mapper;

import com.example.demo.entity.SampleEntity;
import com.example.demo.vo.SampleVO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SampleEntityToVO {
    SampleVO toVo(SampleEntity entity);
    SampleEntity toEntity(SampleVO sampleVO);
}
